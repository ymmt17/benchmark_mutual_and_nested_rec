NFUN = ARGV[0].to_i
NLOOP = ARGV[1].to_i

File.open("./mutual.ml", "w") do |handler|
  handler.puts "let rec f0 = function 0 -> 1 | n -> f1 (n - 1)"
  (1...NFUN).each { |i| handler.puts "and f#{i} n = f#{(i + 1) % NFUN} (n - 1)" }
  handler.puts
  handler.puts "let () = print_int (f0 #{NFUN * NLOOP})"
end

File.open("./nested.ml", "w") do |handler|
  handler.puts "let rec f0 = function 0 -> 1 | n ->"
  (1...NFUN).each { |i| handler.puts "let rec f#{i} n =" }
  handler.puts "f0 (n - 1)"
  (NFUN-1).downto(1) { |i| handler.puts "in f#{i} (n - 1)" }
  handler.puts
  handler.puts "let () = print_int (f0 #{NFUN * NLOOP})"
end
