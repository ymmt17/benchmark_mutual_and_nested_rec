#!/bin/bash

n_fun=$1
n_loop=$2

ruby gen.rb $n_fun $n_loop

ocamlopt -o mutual_native mutual.ml
ocamlopt -o nested_native nested.ml
ocamlc -o mutual_byte mutual.ml
ocamlc -o nested_byte nested.ml

echo "Mutual recursion (Native)"
time ./mutual_native

echo "Nested recursion (Native)"
time ./nested_native

echo "Mutual recursion (Bytecode)"
time ./mutual_byte

echo "Nested recursion (Bytecode)"
time ./nested_byte
